class KeyState
    attr_accessor :keys, :all_keys

    def initialize
        @keys = []
    end

    def update
    end

    def remove_key(id)
        @keys.keep_if { |k| k[:id] != id }
    end

    def add_key(id)
        @keys.push({ :id => id, :down => true })
    end

    def hold_keys(buttons)
        buttons.each { |k|
            if Gosu::button_down? k
                add_key(k)
                return true
            else
                remove_key(k)
            end
        }

        return false
    end

    def hold_key(button)
        hold_keys([button])
    end

    def press_keys(buttons)
        buttons.each { |k|
            if Gosu::button_down? k
                if !@keys.any? { |key| key[:id] == k }
                    add_key(k)
                    return true
                end
            else
                remove_key(k)
            end
        }

        return false
    end

    def press_key(button)
        press_keys([button])
    end
end
