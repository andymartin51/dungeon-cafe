class MenuOption
    attr_accessor :name, :selected, :image

    def initialize(name, selected = false, disabled = false, data = {}, image = nil)
        @name = name
        @selected = selected
        @disabled = disabled
        @data = data
        @image = image.nil? ? Gosu::Image.from_text(@name, 24) : image
    end

    def to_hash
        hash = Hash.new
        hash[:name] = @name
        hash[:selected] = @selected
        hash[:data] = @data
        hash[:image] = @image
        return hash
    end
end
