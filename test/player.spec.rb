require_relative "../player"
gem "minitest"
require "minitest/autorun"

describe Player do
    before do
        @image = { :sprite => "images/player.png", :height => 64, :width => 32 }
        @name = "john"
        @position = { :x => 0, :y => 0, :z => 10 }

        @player = Player.new(@image[:sprite], @name)
        @mock = MiniTest::Mock.new
        $key_state = KeyState.new
        $screen = { :width => 1280, :height => 720, :fullscreen => false }
    end

    describe "initialize" do
        it "should set the sprite" do
            refute_nil @player.image
            assert_equal @player.image.width, @image[:width]
            assert_equal @player.image.height, @image[:height]
        end

        it "should set the name" do
            assert_equal @player.name, @name
        end

        it "should initialize the position" do
            assert_equal @player.position[:x], @position[:x]
            assert_equal @player.position[:y], @position[:y]
            assert_equal @player.position[:z], @position[:z]
        end
    end

    describe "update" do
        it "should call #update" do
            assert_respond_to @player, :update
        end

        it "should call update movement method" do
            @mock.expect :call, nil, []
            player_stub = Player.new(@image[:sprite], @name)
            player_stub.stub :update_movement, @mock do
                player_stub.update
            end
            @mock.verify
        end
    end

    describe "draw" do
        it "should call #draw" do
            assert_respond_to @player, :draw
        end
    end

    describe "move" do
        before do
            @new_position = { :x => 40, :y => 50 }
        end

        it "should call #move" do
            assert_respond_to @player, :move
        end

        it "should update position x and y" do
            @player.move(@new_position[:x], @new_position[:y])
            assert_equal @player.position[:x], @new_position[:x]
            assert_equal @player.position[:y], @new_position[:y]
        end
    end
end
