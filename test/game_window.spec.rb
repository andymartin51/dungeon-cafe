require_relative "../game_window"
gem "minitest"
require "minitest/autorun"

describe GameWindow do
    before do
        @game_title = "Dungeon Cafe"

        @gamewindow = GameWindow.new
    end

    describe "initialize" do
        it "should set caption" do
            assert_equal @gamewindow.caption, @game_title
        end
    end

    describe "update" do
        it "should call #update" do
            @gamewindow.must_respond_to :update
        end
    end

    describe "draw" do
        it "should call #draw" do
            @gamewindow.must_respond_to :draw
        end
    end
end
