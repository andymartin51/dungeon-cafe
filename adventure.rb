class Adventure
    def initialize
        @player = $repo_game_data.player

        # build_tile_map
        $repo_map.build_rand_map

        # warp player to starting or saved position
        # TODO: this will change when player starting position is added
        warp_player(@player.position[:x], @player.position[:y])
    end

    def update
        @player.update

        update_movement

        $repo_game_data.save_game_data
    end

    def draw
        @player.draw

        $repo_map.tiles.each { |tile|
            tile[:image].draw(tile[:coord][:x] * 32, tile[:coord][:y] * 32, tile[:coord][:z])
        }
    end

    def warp_player(x, y)
        $repo_map.tiles.each { |tile|
            tile[:coord][:x] += x
            tile[:coord][:y] += y
        }
    end

    def update_movement
        # move down
        if $key_state.press_keys([Gosu::KbDown, Gosu::KbS, Gosu::KbNumpad2])
            move_player(0, -1)
        end

        # move up
        if $key_state.press_keys([Gosu::KbUp, Gosu::KbW, Gosu::KbNumpad8])
            move_player(0, 1)
        end

        # move left
        if $key_state.press_keys([Gosu::KbLeft, Gosu::KbA, Gosu::KbNumpad4])
            move_player(1, 0)
        end

        # move right
        if $key_state.press_keys([Gosu::KbRight, Gosu::KbD, Gosu::KbNumpad6])
            move_player(-1, 0)
        end

        # move down-right
        if $key_state.press_key Gosu::KbNumpad3
            move_player(-1, -1)
        end

        # move down-left
        if $key_state.press_key Gosu::KbNumpad1
            move_player(1, -1)
        end

        # move up-left
        if $key_state.press_key Gosu::KbNumpad7
            move_player(1, 1)
        end

        # move up-right
        if $key_state.press_key Gosu::KbNumpad9
            move_player(-1, 1)
        end
    end

    def move_player(x, y)
        $repo_map.tiles.each { |tile|
            tile[:coord][:x] += x
            tile[:coord][:y] += y
        }

        @player.position[:x] += x
        @player.position[:y] += y
    end

    def build_tile_map(size_x = 50, size_y = 28)
        @tiles = []
        tiles_overhanging = 10
        size_x = $screen[:width] / 32 + tiles_overhanging
        size_y = $screen[:height] / 32 + tiles_overhanging
        tile_map = { :x => size_x, :y => size_y }

        tile_image = Gosu::Image.new("images/grass_tile_bordered.png")
        tile_image_2 = Gosu::Image.new("images/grass_tile.png")

        tile_map[:y].times { |y|
            tile_map[:x].times { |x|
                tile = { coord: {x: x - tiles_overhanging / 2, y: y - tiles_overhanging / 2, z: 1}, data: {}, image: tile_image }

                if $random.rand(1..2).even?
                    tile[:image] = tile_image_2
                end

                @tiles.push(tile)
            }
        }
    end

    def show_status
        # TODO
    end

    def add_data_to_tile
        # TODO
    end
end
