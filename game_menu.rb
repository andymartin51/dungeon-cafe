require "./menu_option"

class GameMenu
    def initialize
        @saves = []

        load_saves

        build_main_menu

        @menu_state = "main"
    end

    def update
        case @menu_state
        when "main"
            update_main_menu
        when "continue"
            update_continue_menu
        end
    end

    def draw
        case @menu_state
        when "main"
            draw_menu(@main_options)
        when "continue"
            draw_menu(@continue_options)
        end
    end

    def build_main_menu
        @main_options = [
            MenuOption.new("New Game", true).to_hash,
            MenuOption.new("Continue").to_hash,
            MenuOption.new("Quit").to_hash
        ]
    end

    def update_main_menu
        control_menu_cursor(@main_options)

        if $key_state.press_keys([Gosu::KbReturn, Gosu::KbEnter])
            @main_options.each_with_index { |option, i|
                if option[:selected] && !option[:disabled]
                    case option[:name]
                    when "New Game"
                        $window.change_game_state(GameStates::Adventure)
                    when "Continue"
                        @menu_state = "continue"
                    when "Quit"
                        close
                    end
                end
            }
        end
    end

    def update_continue_menu
        control_menu_cursor(@continue_options)

        if $key_state.press_keys([Gosu::KbReturn, Gosu::KbEnter])
            chosen = @continue_options.select{ |save| save[:selected] }.first
            if chosen == @continue_options.last
                @menu_state = "main"
            else
                $repo_game_data.load_player_data(chosen[:data][:id])
                $window.change_game_state(chosen[:data][:game_state])
            end
        end

        if $key_state.press_key(Gosu::KbEscape)
            @menu_state = "main"
        end
    end

    def control_menu_cursor(options)
        options.each_with_index { |option, i|
            if option[:selected]
                if $key_state.press_keys([Gosu::KbDown, Gosu::KbNumpad2, Gosu::KbS])
                    option[:selected] = false
                    if i == options.count - 1
                        options.first[:selected] = true
                    else
                        options[i + 1][:selected] = true
                    end
                end
                if $key_state.press_keys([Gosu::KbUp, Gosu::KbNumpad8, Gosu::KbW])
                    option[:selected] = false
                    if i == 0
                        options.last[:selected] = true
                    else
                        options[i - 1][:selected] = true
                    end
                end
            end
        }
    end

    def draw_menu(options)
        options.each_with_index { |option, i|
            if option[:selected]
                color = 0xff_ffff00
            elsif option[:disabled]
                color = 0xff_777777
            else
                color = 0xff_ffffff
            end
            option[:image].draw($screen[:width] / 2, 10 + 28 * i, 1, 1, 1, color)
        }
    end

    def load_saves
        @saves = $repo_game_data.load_ref_files

        if @saves.size > 0
            @continue_options = @saves.map.with_index { |save, i|
                image = Gosu::Image.from_text(save[:name] + " (" + save[:moment] + ")", 24)
                hash = MenuOption.new(save[:name], false, false, save, image).to_hash
                hash[:selected] = true if i == 0
                hash
            }
            @continue_options.first[:selected] = true
            @continue_options.push(MenuOption.new("Back to Main Menu").to_hash)
        end
    end
end
