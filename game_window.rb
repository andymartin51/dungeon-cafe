require "rubygems"
require "gosu"

require "./key_state"
require "./game_states"
require "./adventure"
require "./game_menu"
require "./repo_game_data"
require "./repo_map"

class GameWindow < Gosu::Window
    attr_accessor :adventure, :game_state

    def initialize(width = 1600, height = 900, fullscreen = false)
        super(width, height, fullscreen)
	    self.caption = "Dungeon Cafe"

        $screen = { :width => width, :height => height, :fullscreen => fullscreen }

        $repo_game_data = RepoGameData.new
        $repo_map = RepoMap.new
        $key_state = KeyState.new
        $random = Random.new

        @game_state = GameStates::MainMenu
        change_game_state(@game_state)
    end

    def update
        case @game_state
        when GameStates::MainMenu
            @main_menu.update
        when GameStates::Adventure
            @adventure.update
        end

        close if $key_state.hold_key(Gosu::KbLeftControl) && $key_state.press_key(Gosu::KbX)

        $key_state.update
    end

    def draw
        case @game_state
        when GameStates::MainMenu
            @main_menu.draw
        when GameStates::Adventure
            @adventure.draw
        end
    end

    def change_game_state(state)
        @game_state = state

        case @game_state
        when GameStates::MainMenu
            @main_menu = GameMenu.new
        when GameStates::Adventure
            @adventure = Adventure.new
        end
    end
end
